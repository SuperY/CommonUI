//
//  BottomLineWiteCenterText.swift
//  CommonUI
//
//  Created by Felix Yuan on 2020/2/25.
//  Copyright © 2020 Hangzhou Shichuo Information Technology Co, Ltd. All rights reserved.
//

import UIKit

open class BottomLineWiteCenterText: NiblessView {
    private var label = UILabel()
    private var leftLineView = UIView()
    private var rightLineView = UIView()

    public override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.hex("F5F5F5")

        label.font = UIFont.PingFangSCRegular(size: 14)
        label.textColor = UIColor.hex("999999")
        leftLineView.backgroundColor = UIColor.hex("DDDDDD")
        rightLineView.backgroundColor = UIColor.hex("DDDDDD")

        add(label, leftLineView, rightLineView)
        label.snp.makeConstraints { make in
            make.center.equalTo(self)
        }

        leftLineView.snp.makeConstraints { make in
            make.height.equalTo(1)
            make.centerY.equalTo(label)
            make.left.equalTo(self).offset(15)
            make.right.equalTo(label.snp.left).offset(-15)
        }

        rightLineView.snp.makeConstraints { make in
            make.height.equalTo(1)
            make.centerY.equalTo(label)
            make.left.equalTo(label.snp.right).offset(15)
            make.right.equalTo(self).offset(-15)
        }
    }

    public func setText(_: String) {
        label.text = "我们的发言有底线"
        sizeToFit()
    }
}
