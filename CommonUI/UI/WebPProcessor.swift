//
//  WebPProcessor.swift
//  CommonUI
//
//  Created by Felix Yuan on 2020/7/9.
//  Copyright © 2020 Hangzhou Shichuo Information Technology Co, Ltd. All rights reserved.
//

import Foundation
import Kingfisher
import libwebp

public struct WebPProcessor: ImageProcessor {

    public static let `default` = WebPProcessor()

    public let identifier = "com.WebPImage.WebPProcessor"

    public init() {}

    public func process(item: ImageProcessItem, options: KingfisherParsedOptionsInfo) -> KFCrossPlatformImage? {
        switch item {
        case .image(let image):
            return image.kf.scaled(to: options.scaleFactor)
        case .data(let data):
            return UIImageWithWebPData(data, options.scaleFactor, nil)
        }
    }
}
