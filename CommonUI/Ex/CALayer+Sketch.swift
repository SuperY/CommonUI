//
//  CALayer+Sketch.swift
//  8btc
//
//  Created by Felix Yuan on 2019/4/22.
//  Copyright © 2019 Hangzhou Shichuo Information Technology Co, Ltd. All rights reserved.
//

import UIKit

public extension CALayer {
    func applySketchShadow(
        color: UIColor = .black,
        alpha: Float = 0.25,
        x: CGFloat = 0,
        y: CGFloat = 3,
        blur: CGFloat = 6,
        spread: CGFloat = 0
    ) {
        shadowColor = color.cgColor
        shadowOpacity = alpha
        shadowOffset = CGSize(width: x, height: y)
        shadowRadius = blur
        if spread == 0 {
            shadowPath = nil
        } else {
            let dx = -spread
            let rect = bounds.insetBy(dx: dx, dy: dx)
            shadowPath = UIBezierPath(rect: rect).cgPath
        }
    }
}
