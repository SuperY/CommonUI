//
//  UITableView+Extension.swift
//  8btc
//
//  Created by Steven Xie on 2018/7/23.
//  Copyright © 2018年 Hangzhou Shichuo Information Technology Co, Ltd. All rights reserved.
//

import UIKit

public extension UITableView {
    /// 下拉刷新 提示条目数
    ///
    /// - Parameters:
    ///   - text: 提示文字
    ///   - delayTime: 停留展示时间
    ///   - height: 条幅高度
    func addHeadTip(text: String?, delayTime: Double, height: CGFloat) {
        let headLabel = UILabel(frame: CGRect(x: 0, y: -height - height - height / 3 * 2, width: UIScreen.main.bounds.size.width, height: height))
        UIView.animate(withDuration: 0.63, animations: {
            headLabel.frame = CGRect(x: 0, y: height, width: UIScreen.main.bounds.size.width, height: height)
            self.tableHeaderView = headLabel
        })

        headLabel.textColor = UIColor.hex("2D8EFF")
        headLabel.backgroundColor = UIColor.hex("CFE6F4")
        headLabel.font = UIFont.systemFont(ofSize: 12)
        headLabel.textAlignment = .center
        headLabel.text = text

        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + delayTime) {
            UIView.animate(withDuration: 0.5, animations: {
                headLabel.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 0.001)
                self.tableHeaderView = headLabel
            }) { _ in
                self.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 0.001))
            }
        }
    }
}
