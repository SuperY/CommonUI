//
//  UIViewController+Extension.swift
//  8btc
//
//  Created by Steven Xie on 2018/11/15.
//  Copyright © 2018 Hangzhou Shichuo Information Technology Co, Ltd. All rights reserved.
//

import MBProgressHUD

public extension UIViewController {
    func hiddenWhenTouchEmptyArea(_ tagGR: UITapGestureRecognizer? = nil) {
        let tap = tagGR ?? UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing(_:)))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    func add(_ child: UIViewController) {
        addChild(child)

        UIView.animate(withDuration: 0.5, delay: 0.1, options: .curveEaseIn, animations: {
            self.tabBarController?.tabBar.isHidden = true
            self.view.addSubview(child.view)
        }) { _ in
            child.didMove(toParent: self)
        }

        child.view.snp.makeConstraints {
            $0.edges.equalTo(self.view)
        }
    }

    func showLoadingHUD() {
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.offset = CGPoint(x: 0, y: 0)
    }

    func hiddentHUD() {
        MBProgressHUD.hide(for: view, animated: true)
    }

    func remove() {
        guard parent != nil else {
            return
        }

        willMove(toParent: nil)

        //        UIView.animate(withDuration: 0.5, delay: 0.1, options: .curveEaseIn, animations: {
        tabBarController?.tabBar.isHidden = false
        //            self.navigationController?.setNavigationBarHidden(false, animated: false)

        //        }) { (finished) in
        removeFromParent()
        view.removeFromSuperview()
        //
        //        }
    }

    func updateNavigationBar(titleColor: UIColor?, tintColor: UIColor?, barTintColor: UIColor? = nil) {
        if let color = titleColor {
            navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: color]
            navigationController?.navigationBar.subviews.forEach { subview in
                if subview is UILabel {
                    (subview as! UILabel).textColor = color
                } else {
                    subview.subviews.forEach { ($0 as? UILabel)?.textColor = color }
                }
            }
        }

        if let color = tintColor {
            navigationController?.navigationBar.tintColor = color
        }

        if let color = barTintColor {
            navigationController?.navigationBar.barTintColor = color
        }
    }

    func addShadowToNavigationBar(color: UIColor? = nil,
                                  shadowOffset: CGSize? = nil,
                                  shadowRadius: CGFloat? = nil,
                                  shadowOpacity: Float? = nil) {
        navigationController?.navigationBar.addShadow(color: color,
                                                      shadowOffset: shadowOffset,
                                                      shadowRadius: shadowRadius,
                                                      shadowOpacity: shadowOpacity)
    }

    func showViewCenter(_ view: UIView, targetView: UIView) {
        if !targetView.subviews.contains(view) {
            targetView.addSubview(view)
            view.snp.makeConstraints {
                $0.center.equalTo(targetView)
                $0.size.equalTo(view.size)
            }
        }
    }
}

// MARK: - 加载 XIB view

public extension UIViewController {
    /// 加载 UIView 类型的 xib
    func loadNib<T>(_: T.Type) -> T where T: UIView {
        return Bundle.main.loadNibNamed("\(T.self)", owner: nil, options: nil)?.first as! T
    }

    /// 加载 UIView 类型的 xib（并设置 frame 参数）
    func loadNib<T>(_ as: T.Type, frame: CGRect) -> T where T: UIView {
        let TView = loadNib(`as`)
        TView.frame = frame
        return TView
    }
}
