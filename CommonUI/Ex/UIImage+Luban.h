//
//  UIImage+Luban_iOS_Extension_h.h
//  8btc
//
//  Created by Steven Xie on 2018/8/1.
//  Copyright © 2018年 Hangzhou Shichuo Information Technology Co, Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Luban_iOS_Extension_h)

+ (NSData *)lubanCompressImage:(UIImage *)image;
+ (NSData *)lubanCompressImage:(UIImage *)image withMask:(NSString *)maskName;
+ (NSData *)lubanCompressImage:(UIImage *)image withCustomImage:(NSString *)imageName;

@end
