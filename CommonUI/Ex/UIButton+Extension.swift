//
//  UIButton+Extension.swift
//  8btc
//
//  Created by Steven Xie on 2018/10/16.
//  Copyright © 2018 Hangzhou Shichuo Information Technology Co, Ltd. All rights reserved.
//

import Kingfisher
import UIKit

public enum ImagePosition: NSInteger {
    case left = 0
    case right = 1
    case top = 2
    case bottom = 3
}

public extension UIButton {
    /// 务必设置了图片才调用此方法
    func setImagePosition(position: ImagePosition, spacing: CGFloat) {
        setTitle(currentTitle, for: .normal)
        setImage(currentImage, for: .normal)

        let imageWidth: CGFloat = (imageView?.image?.size.width)!
        let imageHeight: CGFloat = (imageView?.image?.size.height)!

        let titleStr: NSString = (titleLabel?.text)! as NSString
        let font: UIFont = titleLabel!.font
        let attributes = [NSAttributedString.Key.font: font]
        let option = NSStringDrawingOptions.usesLineFragmentOrigin
        let rect: CGRect = titleStr.boundingRect(with: CGSize(width: LONG_MAX, height: LONG_MAX), options: option, attributes: attributes, context: nil)

        let labelWidth: CGFloat = rect.width
        let labelHeight: CGFloat = rect.height

        let imageOffsetX: CGFloat = (imageWidth + labelWidth) / 2.0 - imageWidth / 2.0
        let imageOffsetY: CGFloat = imageHeight / 2.0 + spacing / 2.0
        let labelOffsetX: CGFloat = (imageWidth + labelWidth / 2.0) - (imageWidth + labelWidth) / 2.0
        let labelOffsetY: CGFloat = labelHeight / 2.0 + spacing / 2.0

        let tempWidth: CGFloat = max(labelWidth, imageWidth)
        let changedWidth: CGFloat = labelWidth + imageWidth - tempWidth
        let tempHeight: CGFloat = max(labelHeight, imageHeight)
        let changedHeight: CGFloat = labelHeight + imageHeight + spacing - tempHeight

        switch position {
        case .left:
            imageEdgeInsets = UIEdgeInsets(top: 0, left: -spacing / 2.0, bottom: 0, right: spacing / 2.0)
            titleEdgeInsets = UIEdgeInsets(top: 0, left: spacing / 2.0, bottom: 0, right: -spacing / 2.0)
            contentEdgeInsets = UIEdgeInsets(top: 0, left: spacing / 2.0, bottom: 0, right: spacing / 2.0)
        case .right:
            imageEdgeInsets = UIEdgeInsets(top: 0, left: labelWidth + spacing / 2.0, bottom: 0, right: -(labelWidth + spacing / 2.0))
            titleEdgeInsets = UIEdgeInsets(top: 0, left: -(imageWidth + spacing / 2), bottom: 0, right: imageWidth + spacing / 2)
            contentEdgeInsets = UIEdgeInsets(top: 0, left: spacing / 2.0, bottom: 0, right: spacing / 2.0)
        case .top:
            imageEdgeInsets = UIEdgeInsets(top: -imageOffsetY, left: imageOffsetX, bottom: imageOffsetY, right: -imageOffsetX)
            titleEdgeInsets = UIEdgeInsets(top: labelOffsetY, left: -labelOffsetX, bottom: -labelOffsetY, right: labelOffsetX)
            contentEdgeInsets = UIEdgeInsets(top: imageOffsetY, left: -changedWidth / 2, bottom: changedHeight - imageOffsetY, right: -changedWidth / 2)
        case .bottom:
            imageEdgeInsets = UIEdgeInsets(top: imageOffsetY, left: imageOffsetX, bottom: -imageOffsetY, right: -imageOffsetX)
            titleEdgeInsets = UIEdgeInsets(top: -labelOffsetY, left: -labelOffsetX, bottom: labelOffsetY, right: labelOffsetX)
            contentEdgeInsets = UIEdgeInsets(top: changedHeight - imageOffsetY, left: -changedWidth / 2, bottom: imageOffsetY, right: -changedWidth / 2)
        }
    }
}
