//
//  UIFont+Extension.swift
//  CommonUI
//
//  Created by Felix Yuan on 2020/2/25.
//  Copyright © 2020 Hangzhou Shichuo Information Technology Co, Ltd. All rights reserved.
//

import UIKit

public extension UIFont {
    static func PingFangSCRegular(size: CGFloat) -> UIFont {
        let font: UIFont? = UIFont(name: "PingFangSC-Regular", size: size)
        return font ?? UIFont.systemFont(ofSize: size)
    }

    static func PingFangSCSemibold(size: CGFloat) -> UIFont {
        let font: UIFont? = UIFont(name: "PingFangSC-Semibold", size: size)
        return font ?? UIFont.systemFont(ofSize: size)
    }

    static func PingFangSCMedium(size: CGFloat) -> UIFont {
        let font: UIFont? = UIFont(name: "PingFangSC-Medium", size: size)
        return font ?? UIFont.systemFont(ofSize: size)
    }

    static func PingFangSCLight(size: CGFloat) -> UIFont {
        let font: UIFont? = UIFont(name: "PingFangSC-Light", size: size)
        return font ?? UIFont.systemFont(ofSize: size)
    }

    static func PingFangSCThin(size: CGFloat) -> UIFont {
        let font: UIFont? = UIFont(name: "PingFangSC-Thin", size: size)
        return font ?? UIFont.systemFont(ofSize: size)
    }

    static func PingFangSCUltralight(size: CGFloat) -> UIFont {
        let font: UIFont? = UIFont(name: "PingFangSC-Ultralight", size: size)
        return font ?? UIFont.systemFont(ofSize: size)
    }

    static func DINRegular(size: CGFloat) -> UIFont {
        let font: UIFont? = UIFont(name: "DIN-Regular", size: size)
        return font ?? UIFont.systemFont(ofSize: size)
    }

    static func DINBold(size: CGFloat) -> UIFont {
        let font: UIFont? = UIFont(name: "DIN-Bold", size: size)
        return font ?? UIFont.systemFont(ofSize: size)
    }

    static func DINMedium(size: CGFloat) -> UIFont {
        let font: UIFont? = UIFont(name: "DIN-Medium", size: size)
        return font ?? UIFont.systemFont(ofSize: size)
    }
}
